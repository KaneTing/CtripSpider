import random
import string
import hashlib

'''
生成16位的md5值,作为照片表的主键
'''
def id_generator(size=16, chars=string.ascii_letters + string.digits):
    str = ''.join(random.choice(chars) for _ in range(size));

    return hashlib.md5("travelgo".join(str).encode('utf-8')).hexdigest()[8:-8];